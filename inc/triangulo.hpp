#ifndef TRIANGULO_H
#define TRIANGULO_H

#include "formaGeometrica.hpp"

class Triangulo : public FormaGeometrica{
	public:
	   Triangulo();
	   float calculaArea();
	   float calculaPerimetro();
};
#endif
