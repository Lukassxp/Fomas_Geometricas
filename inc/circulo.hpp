#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formaGeometrica.hpp"

class Circulo : public FormaGeometrica{
	private:
		float raio;

	public:
		Circulo();
		void setRaio(float raio);
		float getRaio();
		float calculaArea();
		float calculaPerimetro();

};

#endif
