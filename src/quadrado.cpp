#include "quadrado.hpp"

Quadrado::Quadrado(){
	lados = 4;
}

void Quadrado::setLado(float lado){
	this->lado = lado;
	altura = lado;
	base = lado;
}

float Quadrado::getLado(){
	return lado;
}

